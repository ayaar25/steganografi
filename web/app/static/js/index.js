function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function getUrlParam(parameter, defaultvalue=null){
    var urlparameter = defaultvalue;
    if(window.location.href.indexOf(parameter) > -1){
        urlparameter = getUrlVars()[parameter];
        }
    return urlparameter;
}

$('input[name=saveas-check]').change(function() {
  if (this.checked) {
    $('input[name=saveas-text]').prop('readonly', false);
    $('input[name=saveas-text]').prop('placeholder', 'Enter new file name');
  } else {
    $('input[name=saveas-text]').prop('readonly', true);
    $('input[name=saveas-text]').prop('placeholder', 'Use original file name');
  }
});

$('.nav-level-2 > .nav-item > .nav-link').click(function() {
  mode = '';
  if ($(this).hasClass('nav-link-hide')) {
    mode = 'hide';
  } else if ($(this).hasClass('nav-link-recover')) {
    mode = 'recover';
  }
  $('input[name=mode]').val(mode)
});

$('document').ready(function() {

	// tab = getUrlParam('t');
  tab_1 = $('input[name=type]').val()
  tab_2 = $('input[name=method]').val()

  // TODO: FIX THESE, USE GET NEXT TIME PERHAPS
  if (tab_1 != '' && tab_2 != '') {
    $('.nav-link').removeClass('active');
    $('.tab-pane').removeClass('show active');
    
    if (tab_1 == 'video') {
      // $('#mainTab a[href="#video-tab"]').tab('show') // Select tab by name
      $('#video-tab').addClass('active');
      $('#video-content').addClass('show active');

      if (tab_2 == 'hide') {
        $('#video-pills-hide-tab').addClass('active')
        $('#video-pills-hide').addClass('show active')
      } else if (tab_2 == 'recover') {
        $('#video-pills-recover-tab').addClass('active')
        $('#video-pills-recover').addClass('show active')
      }
    } else if (tab_1 == 'audio') {
      // $('#mainTab a[href="#audio-tab"]').tab('show') // Select tab by name
      $('#audio-tab').addClass('active');
      $('#audio-content').addClass('show active');
    
      if (tab_2 == 'hide') {
        $('#audio-pills-hide-tab').addClass('active')
        $('#audio-pills-hide').addClass('show active')
      } else if (tab_2 == 'recover') {
        $('#audio-pills-recover-tab').addClass('active')
        $('#audio-pills-recover').addClass('show active')
      }
    }
  }

  // TODO: CHECK ENCRYPT DISABLE 

  // TODO: DISABLE FIELDSET

  // TODO: POPUP FOR RENAMING (NO NEED WRITE FORMAT)
})