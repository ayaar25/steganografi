import os
import wave
import re
import operator as op
import random
import string
import numpy

def is_char(char, option='uppercase'):
    char_val = ord(char)
    if (option == 'uppercase'):
        return (64 < char_val < 91)
    elif (option == 'lowercase'):
        return (96 < char_val < 122)
    elif (option == 'all_case'):
        return (64 < char_val < 91 or 96 < char_val < 122)
    elif (option == 'all'):
        return (0 < char_val < 255)

def remove_punctuation(text):
	translator = str.maketrans('', '', string.punctuation)
	new_text = text.translate(translator)
	return new_text

def remove_whitespace(text):
	translator = str.maketrans('', '', string.whitespace)
	new_text = text.translate(translator)
	return new_text

def remove_chars(text, chars):
	translator = str.maketrans('', '', chars)
	new_text = text.translate(translator)
	return new_text

def chunk_text(text, size):
	text = remove_whitespace(text)
	text = remove_punctuation(text)
	new_text = ' '.join([text[i:i+size] for i in range(0, len(text), size)])
	return new_text

class VignereExtended:
    
	def __init__(self):
		self.charnum = 256
		self.padding_mode = 'all'

	def read_file(self, filename):
		data = None
		with open(filename, 'rb') as file:
			data = file.read()
		return data

	def write_file(self, filename, data):
		with open(filename, 'wb') as file:
			file.write(data)

	def preprocess(self, texts):
		new_texts = []
		for text in texts:
			if (self.padding_mode == 'all'):
				text = str.encode(text)
			new_texts.append(text)
		return new_texts

	def substitute_char(self, pchar, kchar, forward=True):
		pchar = pchar
		kchar = kchar

		cchar = None
		if (forward):
			cchar = pchar + kchar
		else:
			cchar = pchar - kchar

		cchar = op.mod(cchar, self.charnum)

		return cchar

	def encrypt(self, filename, key):
		cdata = bytearray()
		
		pdata = self.read_file(filename)
		key = self.preprocess([key])[0]

		len_pdata = len(pdata)
		len_key = len(key)

		key_idx = 0
		for cchar in pdata:
			j = op.mod(key_idx, len_key)
			cchar = self.substitute_char(cchar, key[j])
			key_idx += 1
			cdata.append(cchar)
		
		self.write_file(filename, cdata)

	def decrypt(self, filename, key):
		pdata = bytearray()

		cdata = self.read_file(filename)
		key = self.preprocess([key])[0]

		len_cdata = len(cdata)
		len_key = len(key)

		key_idx = 0
		for pchar in cdata:
			j = op.mod(key_idx, len_key)
			pchar = self.substitute_char(pchar, key[j], forward=False)
			key_idx += 1
			pdata.append(pchar)

		self.write_file(filename, pdata)

class AudioSteganography(object):
    def __init__(self):
        pass

    def hide_message(self, file_audio, file_message, file_result, key, encrypted, sequential):
        # Encrypt file_message
        if encrypted :
            ve = VignereExtended()
            ve.encrypt(file_message, key)

        # Read file_message as bits
        bytes_message = numpy.fromfile(file_message, dtype='uint8')
        bits = numpy.unpackbits(bytes_message).tolist()

        # Get file_message format extension
        regex = re.compile('.\w+').findall(file_message)
        format_message = regex[len(regex)-1][1:4]

        bits_format = list(map(int, ''.join([bin(ord(i)).lstrip('0b').rjust(8,'0') for i in format_message])))
        
        # Read file_audio as bytes per frame
        audio = wave.open(file_audio, mode='rb')
        frame_bytes = bytearray(list(audio.readframes(audio.getnframes())))
        
        # Check payload
        if len(bits)*8 > (len(frame_bytes)-58)/8 :
            print(len(bits), "KEGEDEAN")
            return -1
        
        len_message = len(bits)
        length_contents = list(map(int, ''.join(bin(len(bits)).lstrip('0b').rjust(8,'0'))))
        if len(length_contents) < 32:
            for i in range(32-len(length_contents)):
                length_contents.insert(i,0)
        else:
            print(len(bits), "KEGEDEAN")
            return -1
        
        # Shuffle order
        order = list(range(58, len(frame_bytes)))    
        if not sequential:
            seed_number = 0
            for char in key :
                seed_number = seed_number + ord(char)
            random.seed(seed_number)
            random.shuffle(order)
        
        # Insert heading bits
        bits.insert(0,encrypted)
        bits.insert(1,sequential)
        for i in range(len(length_contents)):
            bits.insert(i+2, length_contents[i])
        
        for i in range(len(bits_format)):
            bits.insert(i+34, bits_format[i])

        # Replace LSB of each bytes to bit of bits message heading and data
        for i in range(0, 58):
            frame_bytes[i] = (frame_bytes[i] & 254) | bits[i]

        i = 0
        j = 58
        while i < len_message:
            frame_bytes[order[i]] = (frame_bytes[order[i]] & 254) | bits[j]
            j+=1
            i+=1
        frame_modified = bytes(frame_bytes)

        # Write bytes to new audio file
        regex = re.compile('.\w+').findall(file_audio)
        with wave.open(file_result+regex[len(regex)-1], 'wb') as fd:
            fd.setparams(audio.getparams())
            fd.writeframes(frame_modified)
        audio.close()

        # Decrypt file_message
        if encrypted :
            ve = VignereExtended()
            ve.decrypt(file_message, key)

        return 0
        
    def extract_message(self, file_audio, file_result, key):
        # Read file_audio as bytes per frame
        audio = wave.open(file_audio, mode='rb')
        frame_bytes = bytearray(list(audio.readframes(audio.getnframes())))
        
        # Extract the LSB of each byte
        extracted = [frame_bytes[i] & 1 for i in range(len(frame_bytes))]
        encrypted = extracted[0]
        sequential = extracted[1]
        
        # Get message data length
        length_contents = int("".join([str(extracted[i]) for i in range(2,34)]) ,2)
        
        # Get message data format
        format_message = "".join(chr(int("".join(map(str,extracted[i:i+8])),2)) for i in range(34,58,8))

        # Get order shuffle sequence
        order = list(range(58, len(frame_bytes)))
        if not sequential:
            seed_number = 0
            for char in key :
                seed_number = seed_number + ord(char)
            random.seed(seed_number)
            random.shuffle(order)
        
        # Get message data
        message = []
        temp = []
        for i in range(length_contents):
            temp.append(str(extracted[order[i]]))
            if len(temp) == 8:
                message.append(int("".join(temp),2))
                temp = []
        message = bytearray(message)
        real_message = bytes(message)

        audio.close()

        # Write bytes to new message file
        with open(file_result+"."+format_message, 'wb') as file:
            file.write(real_message)
        file.close()

        # Decrypt file_message
        if encrypted:
            ve = VignereExtended()
            ve.decrypt(file_result+"."+format_message, key)

        return format_message


if __name__ == '__main__' :
    audio = AudioSteganography()
    audio.hide_message(file_audio="audio/dance_to_this_trim.wav", file_message="message/spidey.txt", file_result="audio/dance_to_this_embedded", key="STEGANO", encrypted=False, sequential=True)
    audio.extract_message(file_audio="audio/dance_to_this_embedded.wav", file_result="result", key="STEGANO")
