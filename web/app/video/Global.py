def generate_extended_n2l():
    n2l = []
    for i in range(256):
        n2l.append(chr(i).encode("UTF-8"))
    return n2l


def generate_extended_l2n():
    l2n = {}
    for i in range(256):
        l2n[chr(i).encode("UTF-8")] = i
    return l2n


EXTENDED_N2L = generate_extended_n2l()
EXTENDED_L2N = generate_extended_l2n()

MODE_ENCRYPTION_PLAIN = 0
MODE_ENCRYPTION_ENCRYPTED = 1

MODE_FRAME_INSERTION_SEQUENTIAL = 0
MODE_FRAME_INSERTION_RANDOM = 1

MODE_PIXEL_INSERTION_SEQUENTIAL = 0
MODE_PIXEL_INSERTION_RANDOM = 1

MODE_LSB_ONE_BIT = 0
MODE_LSB_TWO_BIT = 1