from video.Video import *
from video.VideoEncoder import *
from video.VideoDecoder import *
from video.IOManager import *

"""
NOTICE
Two things need to be improved. To-Do for Yesa.
1) Pixel format rbg24 for saving. Maybe problem with Mac.
2) For now, RANDOM FRAME combined with RANDOM PIXEL does not work yet. Currently working on it.
"""

"""
@:param video_input
  string. path to video which will be encoded, i.e. res/video_input.avi
@:param video_output
  string. path to video which is the result of encoding, i.e. res/video_output.mp4
@:param message_input
  string. path to message file which will be embedded into video, i.e. res/inputs/message.txt, res/inputs/message.png
@:param mode_encryption
  either 0 or 1. 0 = without encryption, 1 = with encryption
@:param mode_frame_insertion
  either 0 or 1. 0 = sequential frame, 1 = random frame
@:param mode_pixel_insertion
  either 0 or 1. 0 = sequential pixel, 1 = random pixel
@:param mode_lsb
  either 0 or 1. 0 = lsb one bit, 1 = lsb two bit
@:param key
  string. key for seed as well as encryption
  
@:return video_output
  string. path to video which is the result of encoding, i.e. res/video_output.mp4
"""
def encode(video_input, video_output, message_input, mode_encryption, mode_frame_insertion, mode_pixel_insertion, mode_lsb, key):
    message, extension = read_file(filename= message_input)

    video = Video(filename= video_input)
    ve = VideoEncoder(
        video= video,
        mode_encryption= mode_encryption,
        mode_frame_insertion= mode_frame_insertion,
        mode_pixel_insertion= mode_pixel_insertion,
        mode_lsb= mode_lsb,
        message=message,
        key= key,
        extension= extension
    )

    try:
        ve.encode()
        ve.save_frames_as_video(video_output)
        return video_output

    except Exception as e:
        print(e)
        return "-1"

"""
@:param video_input
  string. path to video which will be decoded, i.e. res/video_input.avi
@:param message_output
  string. path to message which is taken from decoded video. 
  REMEMBER. GIVE PATH WITHOUT EXTENSION BECAUSE THE EXTENSION WILL BE PROVIDED AUTOMATICALLY
  i.e res/outputs/message
@:param key
  string. key for seed as well as decryption
  
@:return message_output
  string. path to message which is taken from decoded video.
  i.e res/outputs/message.txt
"""
def decode(video_input, message_output, key):
    video = Video(filename= video_input)
    vd = VideoDecoder(video= video, key= key)
    write_file(message_output, vd.EXTENSION, vd.decode())

    return message_output + "." + vd.EXTENSION

"""
Code below is only for testing
"""
# encode(
#     video_input="res/video.avi",
#     video_output="res/video_output.avi",
#     message_input="res/inputs/sample_1.txt",
#     mode_encryption=1,
#     mode_frame_insertion=1,
#     mode_pixel_insertion=1,
#     mode_lsb=0,
#     key="ManUnited"
# )
#
# decode(
#     video_input="res/video_output.avi",
#     message_output="res/outputs/sample_1",
#     key="ManUnited"
# )