import random

from video.BinaryConverter import *
from PIL import Image

class ImageDecoder:

    binary_converter = BinaryConverter()

    def __init__(self, image, mode_pixel_insertion, mode_lsb, key, bytes_order=None):
        try:
            self.IMAGE = Image.fromarray(image)
        except AttributeError:
            self.IMAGE = Image.open(image).convert("RGB")
        self.WIDTH, self.HEIGHT = self.IMAGE.size
        self.PIXELS = self.IMAGE.load()

        self.MODE_PIXEL_INSERTION = mode_pixel_insertion
        self.MODE_LSB = mode_lsb
        self.KEY = key

        self.BYTES_ORDER = bytes_order if bytes_order is not None else self.get_bytes_order()

    def get_bytes_order(self):
        bytes_order = []
        if self.MODE_PIXEL_INSERTION == MODE_PIXEL_INSERTION_RANDOM:
            for i in range(0, self.WIDTH * self.HEIGHT):
                bytes_order.append(i)
            random.seed(self.KEY)
            random.shuffle(bytes_order)

        elif self.MODE_PIXEL_INSERTION == MODE_PIXEL_INSERTION_SEQUENTIAL:
            for i in range(0, self.WIDTH * self.HEIGHT):
                bytes_order.append(i)

        return bytes_order

    def get_1d_array_of_pixels(self):
        result = []
        for i in range(self.HEIGHT):
            for j in range(self.WIDTH):
                result.append(self.PIXELS[j, i])
        return result

    def get_binary_pixels_from_pixels(self):
        result = []
        pixels = self.get_1d_array_of_pixels()
        for pixel in pixels:
            result.append(self.binary_converter.get_binary_from_int(pixel[0]))
            result.append(self.binary_converter.get_binary_from_int(pixel[1]))
            result.append(self.binary_converter.get_binary_from_int(pixel[2]))
        return result

    def decode(self):
        binary_pixels = self.get_binary_pixels_from_pixels()
        binary_message = ""

        if self.MODE_LSB is MODE_LSB_ONE_BIT:
            for i in range(0, len(self.BYTES_ORDER)):
                binary_message = binary_message + binary_pixels[self.BYTES_ORDER[i]][-1:]

        elif self.MODE_LSB is MODE_LSB_TWO_BIT:
            for i in range(0, len(self.BYTES_ORDER)):
                binary_message = binary_message + binary_pixels[self.BYTES_ORDER[i]][-2:]

        binary_message = self.binary_converter.separate_binary_array(binary_message)
        message = self.binary_converter.get_string_from_binary_array(
            self.binary_converter.unify_binary_array(binary_message)
        )
        return message