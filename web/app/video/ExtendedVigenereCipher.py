from video.AsciiConverter import *

class ExtendedVigenereCipher:

    ascii_converter = AsciiConverter()

    def p2c(self, plain_char, key_char):
        return (self.ascii_converter.get_ascii_index_from_char(plain_char) + key_char) % 256

    def c2p(self, cipher_char, key_char):
        return (self.ascii_converter.get_ascii_index_from_char(cipher_char) - key_char) % 256

    def get_full_key(self, text, key):
        result = []
        for index in range(len(text)):
            result.append(ord(key[index % len(key)]))
        return result

    def encode(self, plain, key):
        key, index = self.get_full_key(plain, key.lower()), 0
        cipher = []
        for char in plain:
            cipher.append(self.p2c(char, key[index]))
            index = index + 1

        cipher = self.ascii_converter.get_string_from_ascii_array(cipher)

        return cipher

    def decode(self, cipher, key):
        key, index = self.get_full_key(cipher, key.lower()), 0
        plain = []
        for char in cipher:
            plain.append(self.c2p(char, key[index]))
            index = index + 1

        plain = self.ascii_converter.get_string_from_ascii_array(plain)

        return plain

if __name__ == "__main__":
    c = ExtendedVigenereCipher()
    # message = "HELLO, WORLD!!! HELLO, WORLD!!! HELLO, WORLD!!! HELLO, WORLD!!! HELLO, WORLD!!! HELLO, WORLD!!! HELLO, WORLD!!!"
    key = "MANUNITED"
    # cipher = c.encode(message, key)
    # print(cipher)
    cipher = "µ¦ºÁ½¼³¿­²­©¹­½¡ÀÃ·°±±¹±°¼Ì½»À©½³µÀ´¸½Çº­©³Áº¸ »¼³º¹¬²­ºÄË´¶¹¥¼ª°¹°Å¸Æ±¨"
    plain = c.decode(cipher, key)
    print(plain)