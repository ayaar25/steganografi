from Global import *

class AsciiConverter:
    def __init__(self):
        pass

    '''
    :parameter
    ascii representation (int)
    :return
    character (char)
    '''
    def get_char_from_ascii_index(self, ascii_index):
        return EXTENDED_N2L[ascii_index]

    '''
    :parameter
    ascii representation (list of int)
    :return
    message (string)
    '''
    def get_string_from_ascii_array(self, ascii_array):
        result = ""
        for index in ascii_array:
            result = result + str(self.get_char_from_ascii_index(index), "UTF-8")
        return result

    '''
    :parameter
    character (char)
    :return
    ascii representation (int)
    '''
    def get_ascii_index_from_char(self, char):
        return EXTENDED_L2N[bytes(char, "UTF-8")]

    ''' 
    :parameter
    message (string)
    :return
    ascii representation (list of int)
    '''
    def get_ascii_array_from_string(self, string):
        result = []
        for char in string:
            result.append(self.get_ascii_index_from_char(char))
        return result

if __name__ == "__main__":
    ac = AsciiConverter()
    message = "HELLO, WORLD!!!"
    ascii_message = ac.get_ascii_array_from_string(message)
    print(ascii_message)
    plain_message = ac.get_string_from_ascii_array(ascii_message)
    print(plain_message)