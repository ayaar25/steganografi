import os
from PIL import Image
import io
from Global import *

EXTENSION_IMAGE = ["jpg", "png"]
EXTENSION_TEXT = ["txt"]

def read_file(filename):
    parts = filename.split(".")
    extension = parts[len(parts) - 1]

    if extension in EXTENSION_IMAGE:
        return read_image(filename), extension
    elif extension in EXTENSION_TEXT:
        return read_text(filename), extension
    else:
        raise Exception("File Unknown")

def write_file(filename, extension, text):
    filename = filename + "." + extension
    if extension in EXTENSION_IMAGE:
        write_image(filename, text)
    elif extension in EXTENSION_TEXT:
        write_text(filename, text)
    else:
        raise Exception("File Unknown")

def read_text(filename):
    file = open(filename, "r")
    text = file.read()
    file.close()
    return text

def write_text(filename, text):
    file = open(filename, "w")
    file.write(text)
    file.flush()
    os.fsync(file.fileno())
    file.close()

def read_image(filename):
    byte = list(open(filename, "rb").read())
    text = ""
    for each in byte:
        text = text + str(EXTENDED_N2L[each], "UTF-8")
    return text

def write_image(filename, text):
    byte = []
    for each in text:
        byte.append(EXTENDED_L2N[bytes(each, "UTF-8")])
    image = Image.open(io.BytesIO(bytes(byte)))
    image.save(filename)