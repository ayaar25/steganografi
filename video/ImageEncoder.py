from BinaryConverter import *
from PIL import Image
import random
import numpy as np

class ImageEncoder:

    binary_converter = BinaryConverter()

    def __init__(self, image, mode_pixel_insertion, mode_lsb, message, key, bytes_order=None):
        try:
            self.IMAGE = Image.fromarray(image)
        except AttributeError:
            self.IMAGE = Image.open(image).convert("RGB")
        self.WIDTH, self.HEIGHT = self.IMAGE.size
        self.PIXELS = self.IMAGE.load()

        self.MODE_PIXEL_INSERTION = mode_pixel_insertion
        self.MODE_LSB = mode_lsb
        self.MESSAGE = message
        self.KEY = key

        self.BYTES_ORDER = bytes_order if bytes_order is not None else self.get_bytes_order()

        self.assert_message_fit_image()

    def assert_message_fit_image(self):
        if len(self.MESSAGE) <= 0:
            raise Exception("Message cannot be empty")
        if self.WIDTH * self.HEIGHT * 3 < 8 * len(self.MESSAGE):
            raise Exception("Message does not fit image")

    def get_bytes_order(self):
        bytes_order = []

        if self.MODE_PIXEL_INSERTION == MODE_PIXEL_INSERTION_RANDOM:
            for i in range(0, self.WIDTH * self.HEIGHT):
                bytes_order.append(i)
            random.seed(self.KEY)
            random.shuffle(bytes_order)

        elif self.MODE_PIXEL_INSERTION == MODE_PIXEL_INSERTION_SEQUENTIAL:
            for i in range(0, self.WIDTH * self.HEIGHT):
                bytes_order.append(i)

        return bytes_order

    def normalize_image(self):
        for i in range(self.HEIGHT):
            for j in range(self.WIDTH):
                self.PIXELS[j, i] = (self.PIXELS[j, i][2], self.PIXELS[j, i][1], self.PIXELS[j, i][0])

    def encode(self):
        # Example of binary message unified     : ["101010"]
        # Example of binary message separated   : ["1", "0", "1", "0", "1", "0"]
        binary_message_unified = self.binary_converter.get_binary_array_from_string(self.MESSAGE)
        binary_message_separated = self.binary_converter.separate_binary_array(binary_message_unified)

        # Example of binary pixels              : ["00000000", "11111111"]
        binary_pixels = self.get_binary_pixels_from_pixels()
        binary_pixels_with_message = self.insert_binary_message_to_binary_pixels(binary_pixels, binary_message_separated)
        pixels = self.get_pixels_from_binary_pixels(binary_pixels_with_message)

        return np.array(pixels, dtype=np.uint8)

    def get_binary_pixels_from_pixels(self):
        result = []
        pixels = self.get_1d_array_of_pixels()
        for pixel in pixels:
            result.append(self.binary_converter.get_binary_from_int(pixel[0]))
            result.append(self.binary_converter.get_binary_from_int(pixel[1]))
            result.append(self.binary_converter.get_binary_from_int(pixel[2]))
        return result

    def get_pixels_from_binary_pixels(self, binary_pixels):
        result = []
        i = 0
        while i < len(binary_pixels):
            channel_0 = self.binary_converter.get_int_from_binary(binary_pixels[i])
            channel_1 = self.binary_converter.get_int_from_binary(binary_pixels[i + 1])
            channel_2 = self.binary_converter.get_int_from_binary(binary_pixels[i + 2])
            result.append((channel_0, channel_1, channel_2))
            i = i + 3
        return self.get_2d_array_of_pixels(result)

    def insert_binary_message_to_binary_pixels(self, binary_pixels, binary_message_separated):
        if self.MODE_LSB is MODE_LSB_ONE_BIT:
            for i in range(len(binary_message_separated)):
                binary_pixels[self.BYTES_ORDER[i]] = binary_pixels[self.BYTES_ORDER[i]][:-1] + binary_message_separated[i]

        elif self.MODE_LSB is MODE_LSB_TWO_BIT:
            i, j = 0, 0
            while i < len(binary_message_separated):
                binary_pixels[self.BYTES_ORDER[j]] = binary_pixels[self.BYTES_ORDER[j]][:-2] + binary_message_separated[i] + binary_message_separated[i + 1]
                i = i + 2
                j = j + 1

        return binary_pixels

    def get_1d_array_of_pixels(self):
        result = []
        for i in range(self.HEIGHT):
            for j in range(self.WIDTH):
                result.append(self.PIXELS[j, i])
        return result

    def get_2d_array_of_pixels(self, serialized_pixels):
        result = [[0 for j in range(self.WIDTH)] for i in range(self.HEIGHT)]
        k = 0
        for i in range(self.HEIGHT):
            for j in range(self.WIDTH):
                result[i][j] = serialized_pixels[k]
                k = k + 1
        return result

    def save_image(self, filename):
        self.IMAGE.save(filename)


if __name__ == "__main__":
    ic = ImageEncoder("res/image_blog.png")
    message = "HELLO, WORLD!!!"
    ic.encode(message)
    ic.save_image("res/image_blog_2.png")

    ic = ImageEncoder("res/image_blog_2.png")
    print(ic.decode())